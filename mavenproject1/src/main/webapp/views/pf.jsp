<%-- 
    Document   : pf
    Created on : 27 oct. 2021, 22:05:04
    Author     : lenovo
--%>
 
<%
// Validaci�n de entrada desde el men� principal
if (request.getParameter("menu") != null){
%>

<br /><br />
 
<div class="columns" >
    <div class="box">

            <div id="carrusel"  class="center">
        <a href="#" class="left-arrow"><img src="./imagen/izquierda.png" /></a>
        <a href="#" class="right-arrow"><img src="./imagen/derecha.png" /></a>
        <div class="carrusel">
            <div class="product" id="product_0">
                <img src="./imagen/Daidouji.jpg" width="195" height="100" />
                <h5>Daidouji</h5>
            </div>
            <div class="product" id="product_1">
                <img src="./imagen/baby-rosalina.jpg" width="195" height="100" />
                <h5>Baby Rosalina 2</h5>
            </div>
            <div class="product" id="product_2">
                <img src="./imagen/pokemon.png" width="195" height="100" />
                <p><h5> Grookey</h5></p>
            </div>
            <div class="product" id="product_3">
                <img src="./imagen/setsuna.jpg" width="195" height="100" />
                <h5>Setsuna</h5>
            </div>
            <div class="product" id="product_4">
                <img src="./imagen/tae.jpg" width="195" height="100" />
                <h5>Yamada Tae</h5>
            </div>
            <div class="product" id="product_5">
                <img src="./imagen/lucina.jpg" width="195" height="100" />
                <h5>Lucina</h5>
            </div>
        </div>
    </div>
            
        </div>
</div>

    
    <br /><br />
  <!--parrafos-->
  <div class="column">
        <div class="box">
      
      <p class="title is-5">Daidouji </p>
      <p class="subtitle">
          Daidouji es un personaje jugable de la serie de videojuegos Senran Kagura. Ella hace su debut en Senran Kagura: Portrait of Girls.
          Es una estudiante de tercer a�o de la Academia Hanzo. Es una estudiante mayor enigm�tica y legendaria, que emana un aura peligrosa.      
          A pesar de su edad y registro de asistencia escasa, ella sigue reinando sobre la Academia. Ella posee una incre�ble velocidad y un poder abrumador, y en contraste con su entrenamiento ninja, nunca se molesta en las t�cticas de sigilo
          Su estilo de lucha hace temblar la tierra y el cielo, centrada en atacar a sus enemigos de frente con su enorme fuerza.
      </p>
      <br />
      <hr />
      
      <p class="title is-5">Beb� Rosalina</p>
      <p class="subtitle">
          Beb� Estela o Beb� Rosalina (en ingl�s: Baby Rosalina) es un personaje que apareci� por primera vez en el juego Mario Kart 8, para la consola de sobremesa Wii U. Aunque apareci� por primera vez en este juego Beb� Estela hab�a sido creada y usada muchas veces por los fans en 
          p�ginas web fanon. Beb� Estela es la hom�loga del pasado de Estela. Su color es el turquesa.
      </p>
      <br />
      <hr />
      
      <p class="title is-5">Grookey</p>
      <p class="subtitle">
          Grookey es un Pok�mon de tipo planta introducido en la octava generaci�n. Es uno de los Pok�mon iniciales de Pok�mon Espada y Pok�mon Escudo
          Grookey es un mono verde con orejas y cola marr�n, manos y hocico de color naranja, y una hoja con una ramita en su cabeza. La baqueta que lleva Grookey no era m�s que una ramita recogida del bosque en el que habita su manada,
          pero, tras estar expuesta a la energ�a que emana de su cuerpo, adquiri� poderes especiales. El pelaje verde de Grookey sintetiza energ�a a partir de la luz solar. Las flores y hojas marchitas recuperan su color cuando Grookey golpea con su baqueta cerca de ellas.
        </p>
      <br />
      <hr />
      
      <p class="title is-5">Setsuna </p>
      <p class="subtitle">
          Setsuna (???) es una de las tres protagonistas de la serie de anime Hany? no Yashahime.
          Es la menor de las gemelas hanyo que nacieron de la relaci�n de Sesshomaru con su esposa[3] humana Rin[4]. Luego de separarse de su hermana sus sue�os fueron consumidos por una Mariposa de los sue�os, por lo que no es capaz de dormir y mucho menos recordar algo de su pasado.
          Mi nombre es Setsuna, y no tengo nada mas que decirte.. Por esa raz�n, este es tu fin? ? Setsuna en el Trailer.
      </p>
      <br />
      <hr />
      
      <p class="title is-5"> Yamada Tae </p>
      <p class="subtitle">
          Yamada Tae (?? ? ?) es una chica misteriosa con un pasado desconocido. Ella es la �nica zombie que no ha despertado. Kotaro no da ninguna explicaci�n a quien es y cuando Sakura le pregunto el solo respondi� "Quien dice que debe haber una leyenda para todo". Tambi�n es miembro del grupo idol Franchouchou.
      La zombie de un largo pelo negro. La �nica zombie de los siete que a�n no est� completamente consciente. Como tal, ella es la m�s zombie del grupo, siempre tratando de morder algo. Aunque Tae solo habla en gemidos, parece ser capaz de entender lo que el resto de los miembros est�n
      diciendo hasta cierto punto. Debido a que ella es impulsada puramente por instinto, Tae siempre termina mordisqueando a Junko cuando est� cerca.    
      </p>
      <br />
      <hr />
      
      <p class="title is-5">Lucina</p>
      <p class="subtitle">
          Lucina es un personaje jugable en Fire Emblem Awakening y una de los tres protagonistas de �ste.
          Lucina es la hija de Chrom del futuro y tiene la marca de Naga, conocida como la marca del venerable, en su ojo izquierdo. Ella es una princesa amable y decidida con un gran sentido de justicia, que cree que salvar al mundo es su misi�n. Ella adora a su padre y siempre se est� 
          preocupando por �l. Su espada es la Falchion linaje que es la misma espada  que tiene Chrom pero tra�da del futuro. Dependiendo con quien se case Chrom, Lucina puede ser hermana de Linfan (masculino), ��igo, Cynthia, Brady o Kjelle, o ser hija �nica. Tambi�n es prima de Owain y posiblemente de Linfan (femenino) si Daraen se casa con Lissa o con Emmeryn.
      </p>
      <br />
      <hr />
      </div>
    
    </div> 
    <br /><br />
    

    <div class="container">
        <div class="row">
            <form class="col s12">
                <div class="card white black-text">
                    <div class="card-content">
              <div class="row">
                <div class="input-field col s6">
                  <i class="material-icons prefix">mode_edit</i>
                  <textarea id="icon_prefix2" class="materialize-textarea"></textarea>
                  <label for="icon_prefix2">Redacte su pinion sobre mi pagina</label>
                </div>
              </div>
              <!--botones-->
              <button class="button is-success" input type="submit" >
                  <span class="icon is-small">
                      <i class="material-icons prefix">done</i>
                  </span>
                  <span>Save</span>
                </button>

                <button class="button is-success" input type="reset">
                    <span>Delete</span>
                    <span class="icon is-small">
                        <i class="material-icons prefix">delete_forever</i>   
                    </span>
                </button>

            </div>
          </div>
            </form>
          </div>
    </div>   
</div>
      </div>

      <% }else {
    // Cargando la P�gina de errores
    String redirectURL = "../index.jsp?menu=401";
    response.sendRedirect(redirectURL);
}%>